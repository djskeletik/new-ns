# import sqlite3
# import os
# import time
#
# database_existed = os.path.exists("NHS.db")
# connection = sqlite3.connect("NHS.db")

import MySQLdb

connection = MySQLdb.connect('localhost', 'root', '12340', 'nhs')
cursor = connection.cursor()

# cursor = connection.cursor()
#
# if not database_existed:
cursor.execute(
    "CREATE TABLE IF NOT EXISTS logins (chat_id varchar(20) PRIMARY KEY, login varchar(40), pass varchar(40), "
    "login_success BOOL DEFAULT FALSE)")
connection.commit()


def create_login(login, password, chat_id, login_success):
    cur = connection.cursor()
    try:
        cur.execute("INSERT INTO logins VALUES (%s, %s, %s, %s)", (chat_id, login, password, login_success))
        connection.commit()
    except MySQLdb.IntegrityError:
        cur.execute("UPDATE logins SET login = %s, pass = %s, login_success=%s WHERE chat_id = %s", (login, password, login_success, chat_id))
        connection.commit()
        return False
    return True


def delete_login(chat_id):
    cur = connection.cursor()
    cur.execute("DELETE FROM logins WHERE chat_id = %s;", (chat_id,))
    connection.commit()


def find_login(chat_id):
    cur = connection.cursor()
    rows = cur.execute("SELECT login, pass FROM logins WHERE chat_id = %s", (chat_id,))
    result = (None, None)
    for (l, p) in cur:
        result = (l, p)
    return result
