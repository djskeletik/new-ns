import telebot
import netschool_parses
from telebot import types
from datetime import date, timedelta
import nhs_database
from os.path import exists
import os
from win32com import client

bot = telebot.TeleBot("1924180502:AAG_4FDliX-WkycKwBJ3OwfIP3NVIypUiRs", threaded=False)
marks_cache = {}


@bot.message_handler(commands=['login'])
def login(message):
    pars = netschool_parses.NetschoolParser()
    try:
        text = message.text.split(" ")
        res = pars.check_login(text[1], text[2])
    except:
        bot.send_message(message.chat.id, "Ошибка запроса")
    else:

        if res:
            create_login = nhs_database.create_login(text[1], text[2], message.chat.id, True)
            if create_login is True:
                bot.send_message(message.chat.id, "Ваш аккаунт успешно добавлен!")

            else:
                bot.send_message(message.chat.id, "Ваш аккаунт был перезаписан!")
        else:
            bot.send_message(message.chat.id, "Повторите попытку")


@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.send_message(message.chat.id,
                     "███╗░░██╗██╗░░██╗░██████╗ \n"
                     "████╗░██║██║░░██║██╔════╝ \n"
                     "██╔██╗██║███████║╚█████╗░ \n"
                     "██║╚████║██╔══██║░╚═══██╗ \n"
                     "██║░╚███║██║░░██║██████╔╝ \n"
                     "╚═╝░░╚══╝╚═╝░░╚═╝╚═════╝░ \n Если что я тут пылатся символами нарисовать слово NHS, но как многие могут заметить для "
                     "телефона не получилось, а на компьютере хорошо видно)\n \n"
                     "Приветствую! Я официальный бот НГШ(новой гуманитарной школы).\nДля того чтобы начать напишите "
                     "/get_marks  (логин)  (пароль) \n"
                     "Пример: /get_marks Иванов 123456\n Также не забудьте пройти опрос о боте "
                     "Не забудьте оценить работу моего бота: https://forms.gle/wkoWjUshrb9ahsUD6\n"
                     )


@bot.message_handler(commands=['get_GPA'])
def get_GPA(message):
    global login, pwd
    (login, pwd) = nhs_database.find_login(message.chat.id)
    if login:
        try:
            parser = netschool_parses.NetschoolParser()
            parser.check_GPA(login, pwd)

        except netschool_parses.LoginException:
            bot.send_message(message.chat.id, "Не удалось получить средний балл, проверьте корректность логина и "
                                              "пароля!")
            return False
        if exists("C:\\Users\\Daniil\\Downloads\\Документ без имени.pdf"):
            f = open('C:/Users/Daniil/Downloads/Документ без имени.pdf', 'rb')
            bot.send_document(message.chat.id, f)
            f.close()
            os.remove('C:/Users/Daniil/Downloads/Документ без имени.pdf')
            bot.send_message(message.chat.id, "Итоговые отметки")
        else:
            bot.send_message(message.chat.id, "Произошла ошибка! Повторите запрос")
        return True
    else:
        bot.send_message(message.chat.id, "Требуется регистрация! Воспользуйтесь командой /login \"Имя\" \"Пароль\"")
        return False


@bot.message_handler(commands=['get_marks'])
def get_marks(message):
    global login, pwd
    (login, pwd) = nhs_database.find_login(message.chat.id)
    if login:
        try:
            parser = netschool_parses.NetschoolParser()
            marks = parser.check_marks(login, pwd)
            marks_cache[message.chat.id] = marks
        except netschool_parses.LoginException:
            bot.send_message(message.chat.id, "Не удалось получить оценки, проверьте корректность логина и пароля!")
            return False
        markup = types.InlineKeyboardMarkup()
        todayB = types.InlineKeyboardButton('Сегодня', callback_data='today')
        yesterdayB = types.InlineKeyboardButton('Вчера', callback_data='yesterday')
        this_weekB = types.InlineKeyboardButton('Эта неделя', callback_data='this_week')
        last_weekB = types.InlineKeyboardButton('Прошлая неделя', callback_data='last_week')
        markup.row(todayB, yesterdayB)
        markup.row(this_weekB)
        markup.row(last_weekB)
        bot.send_message(message.chat.id, "За какой период вы хоите получить оценки?", reply_markup=markup)
        return True
    else:
        bot.send_message(message.chat.id, "Требуется регистрация! Воспользуйтесь командой /login \"Имя\" \"Пароль\"")
        return False


@bot.message_handler(commands=["help"])
def help(message):
    bot.send_message(message.chat.id, "1)/start - начальная команда, при которой запускается бот \n"
                                      "2)/help - выводит список команд              \n"
                                      "3)/login (логин) (пароль) - регистрирует вас в базе данных бота \n"
                                      "Пример: /login Иванов 123456               \n"
                                      "4)/get_marks (логин) (пароль) - выводит оценки\n"
                                      "Пример: /get_marks Иванов 123456           \n"
                                      "Но если вы уже зарегенстрировались,то можете писать просто /get_marks \n")


@bot.callback_query_handler(func=lambda call: True)
def test_callback(call):
    try:
        if marks_cache is None:
            bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки")
            return False
        if call.data == "today":
            bot.send_message(call.message.chat.id, "Сегодня:")
            n = []
            for i in marks_cache[call.message.chat.id]:
                today = date.today()
                ns_today = i["date"].split('.')
                if i["Оценка"] == "-":
                    continue
                if ns_today[0] == f"{today.day}":
                    n.append(i['предмет'] + ": Вес " + i['Вес'] + " Оценка " + i['Оценка'])
                if n is None:
                    bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки")
            bot.send_message(call.message.chat.id, "\n".join(n))
        elif call.data == "yesterday":
            bot.send_message(call.message.chat.id, "Вчера:")
            n = []
            for i in marks_cache[call.message.chat.id]:
                yesterday = date.today() - timedelta(days=1)
                ns_yesterday = i["date"].split('.')
                if i["Оценка"] == "-":
                    continue
                if ns_yesterday[0] == f"{yesterday.day}":
                    n.append(i['предмет'] + ": Вес " + i['Вес'] + " Оценка " + i['Оценка'])
                if n is None:
                    bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки")
            bot.send_message(call.message.chat.id, "\n".join(n))
        elif call.data == "this_week":
            bot.send_message(call.message.chat.id, "Эта неделя:")
            n = []
            for i in marks_cache[call.message.chat.id]:
                if i["Оценка"] == "-":
                    continue
                if i.get("week") == 0:
                    n.append(i['предмет'] + ": Вес " + i['Вес'] + " Оценка " + i['Оценка'])
                if n is None:
                    bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки")
            bot.send_message(call.message.chat.id, "\n".join(n))
        elif call.data == "last_week":
            n = []
            bot.send_message(call.message.chat.id, "Прошлая неделя:")
            for i in marks_cache[call.message.chat.id]:
                if i["Оценка"] == "-":
                    continue
                if i.get("week") == 1:
                    n.append(i['предмет'] + ": Вес " + i['Вес'] + " Оценка " + i['Оценка'])
                if n is None:
                    bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки")
            bot.send_message(call.message.chat.id, "\n".join(n))
    except:
        bot.send_message(call.message.chat.id, "Ошибка, вас заскамили на оценки, либо запрос устарел")


bot.polling()
