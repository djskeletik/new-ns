from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.options import Options
from threading import Lock
from selenium.webdriver.common.by import By
import base64
import json

my_lock = Lock()


class LoginException(Exception):
    pass


class Exception404(Exception):
    pass


class NetschoolParser:
    def __init__(self):
        self.nhs_network = 'home'
        # self.networks = subprocess.check_output(["netsh", "wlan", "show", "interfaces"]).decode("cp437")
        # for network in self.networks.split("\n"):
        #     if network.strip().split(" ")[0] == "SSID:":
        #         self.nhs_network = network.split()[1]
        #         # print("ok")
        #     else:
        #         pass

    def check_GPA(self, login, pwd):
        with my_lock:
            options = Options()
            appState = {
                "recentDestinations": [
                    {
                        "id": "Save as PDF",
                        "origin": "local",
                        "account": ""
                    }
                ],
                "selectedDestinationId": "Save as PDF",
                "version": 2
            }
            prefs = {
                'printing.print_preview_sticky_settings.appState': json.dumps(appState)}
            options.add_experimental_option('prefs', prefs)
            options.add_argument('--kiosk-printing')
            # options.add_argument('--headless')
            driver = webdriver.Chrome(options=options)
            driver.set_window_size(1600, 1200)
            if self.nhs_network == "nhs":
                driver.get("http://192.168.1.1/asp/Announce/ViewAnnouncements.asp")
            else:
                driver.get("http://91.200.226.70/")
            self._login_to_netschool(driver, login, pwd)
            button_1 = driver.find_element(By.CSS_SELECTOR, "body > div.header > div.navbar.navbar-default > nav > ul "
                                                            "> li:nth-child(2) > a")
            button_1.click()
            button_2 = driver.find_element(By.CSS_SELECTOR, "body > div.header > div.navbar.navbar-default > nav > ul "
                                                            "> li:nth-child(2) > ul > li:nth-child(2) > a")
            button_2.click()
            button_3 = driver.find_element(By.CSS_SELECTOR, "body > div.block-content > div.content > div > div > div "
                                                            "> table > tbody > tr:nth-child(2) > td:nth-child(2) > a")
            button_3.click()
            button_4 = driver.find_element(By.CSS_SELECTOR, "#buttonPanel > div.buttons-panel-left > button")
            button_4.click()
            WebDriverWait(driver, 5).until(
                lambda x: x.find_element(By.CSS_SELECTOR, "#report > div > table.table-print-num"))

            # button_5 = driver.find_element(By.CSS_SELECTOR, "#actionPanel > div > button")
            button_5 = driver.find_element(By.CSS_SELECTOR, "#actionPanel > button:nth-child(1)")
            button_5.click()

            time.sleep(3)

    def check_marks(self, login, password):
        with my_lock:
            driver = webdriver.Chrome()
            if self.nhs_network == "nhs":
                driver.get("http://192.168.1.1/asp/Announce/ViewAnnouncements.asp")
            else:
                driver.get("http://91.200.226.70/")
            self._login_to_netschool(driver, login, password)
            button_journal = driver.find_element(By.CSS_SELECTOR,
                                                 "body > div.header > div.navbar.navbar-default > nav > ul > "
                                                 "li:nth-child(3) > a")
            button_journal.click()
            button_journal2 = driver.find_element(By.CSS_SELECTOR,
                                                  "body > div.header > div.navbar.navbar-default > nav > ul > "
                                                  "li:nth-child(3) > ul > li:nth-child(2) > a")
            button_journal2.click()

            WebDriverWait(driver, 5).until(
                EC.title_contains("Дневник")
            )

            time.sleep(3)
            marks = []

            for i in range(0, 2):
                marks_table = driver.find_element(By.TAG_NAME, "tbody")
                rows = marks_table.find_elements(By.TAG_NAME, "tr")
                if len(rows) <= 2:
                    continue
                rows.pop(0)
                rows.pop(0)
                current_date = None
                for row in rows:
                    if row.get_attribute("valign") == "top":
                        continue
                    if row.get_attribute("class") == "visible-scr-row-sm":
                        continue
                    tds = row.find_elements_by_tag_name("td")
                    if tds[0].get_attribute("class") == "hidden-scr-sm":
                        current_date = tds[0].find_element_by_tag_name("a").get_attribute("innerHTML")
                        tds.pop(0)
                    if tds[-1].get_attribute("innerHTML") == "-" and tds[1].get_attribute("innerHTML") != "Д":
                        continue
                    marks.append({
                        "предмет": tds[0].get_attribute("innerHTML"),
                        "Вес": tds[3].get_attribute("innerHTML").strip(),
                        "Дз": tds[2].find_element_by_tag_name("a").get_attribute("innerHTML"),
                        "Оценка": tds[-1].get_attribute("innerHTML"),
                        "date": current_date,
                        "week": i
                    })

                if i == 0:
                    week_button = driver.find_element(By.XPATH, "/html/body/div[2]/div["
                                                                "1]/div/div/div/form/div/div[2]/div/div["
                                                                "1]/div/div/span/button[1]")
                    week_button.click()
                    time.sleep(3)

            driver.quit()
            return marks

    def _login_to_netschool(self, driver, login, password):
        login_field_UN = driver.find_element(By.NAME, "UN")
        login_field_UN.send_keys(login)
        login_field_PW = driver.find_element(By.NAME, "PW")
        login_field_PW.send_keys(password)
        button_login = driver.find_element(By.CLASS_NAME, "button-login")
        button_login.click()
        time.sleep(3)
        #
        # try:
        #     # WebDriverWait(driver, 2).until(
        #     #     EC.url_contains("http://91.200.226.70/asp/Announce/ViewAnnouncements.asp")
        #     #     or EC.url_contains("http://91.200.226.70/asp/Curriculum/Assignments.asp")
        #     # )
        #
        # except TimeoutException:
        #     print(driver.current_url)
        if driver.current_url == "http://91.200.226.70/asp/SecurityWarning.asp":
            button_con = driver.find_element(By.CSS_SELECTOR, "body > div.block-content > div > div > div > div > "
                                                              "div:nth-child(5) > div > div > div > div > "
                                                              "button:nth-child(2)")
            button_con.click()
        try:
            WebDriverWait(driver, 5).until(
                EC.url_contains("http://91.200.226.70/asp/SetupSchool/CreateSecretAnswerToQuestion.asp")
            )
        except TimeoutException:
            return True
        else:
            qw = driver.find_element(By.CSS_SELECTOR,
                                     "body > div.block-content > div > div > div > div > div > div > div > "
                                     "button.btn.btn-default")
            print(qw)
            qw.click()
            return True

    def check_login(self, login, pwd):
        driver_ = webdriver.Chrome()
        driver_.get("http://91.200.226.70/")
        res = self._login_to_netschool(driver_, login, pwd)
        return res

    def HW(self, login, password):
        with my_lock:
            driver = webdriver.Chrome()
            if self.nhs_network == "nhs":
                driver.get("http://192.168.1.1/asp/Announce/ViewAnnouncements.asp")
            else:
                driver.get("http://91.200.226.70/")
            # http://192.168.1.1/asp/Announce/ViewAnnouncements.asp
            self._login_to_netschool(driver, login, password)
            button_journal = driver.find_element_by_css_selector(
                "body > div.header > div.navbar.navbar-default > nav > ul > "
                "li:nth-child(3) > a")
            button_journal.click()
            button_journal2 = driver.find_element_by_css_selector(
                "body > div.header > div.navbar.navbar-default > nav > ul > "
                "li:nth-child(3) > ul > li:nth-child(2) > a")
            button_journal2.click()

            WebDriverWait(driver, 5).until(
                EC.title_contains("Дневник")
            )

            time.sleep(3)
            HW = []

            for i in range(0, 2):
                HW_table = driver.find_element_by_tag_name("tbody")
                rows = HW_table.find_elements_by_tag_name("tr")
                if len(rows) <= 2:
                    continue
                rows.pop(0)
                rows.pop(0)
                current_date = None
                for row in rows:
                    if row.get_attribute("valign") == "top":
                        continue
                    if row.get_attribute("class") == "visible-scr-row-sm":
                        continue
                    tds = row.find_elements_by_tag_name("td")
                    if tds[0].get_attribute("class") == "hidden-scr-sm":
                        current_date = tds[0].find_element_by_tag_name("a").get_attribute("innerHTML")
                        tds.pop(0)
                    if tds[2].get_attribute("innerHTML") != "Д":
                        continue
                    HW.append({
                        "Задание": tds[0].get_attribute("innerHTML"),
                        "Файл": None,
                        "date": current_date,
                        "week": i
                    })

                if i == 0:
                    week_button = driver.find_element_by_xpath("/html/body/div[2]/div["
                                                               "1]/div/div/div/form/div/div[2]/div/div["
                                                               "1]/div/div/span/button[1]")
                    week_button.click()
                    time.sleep(3)

            driver.quit()
            return HW


if __name__ == "__main__":
    NetschoolParser().check_GPA("Теслюк", "2122232425")
